package com.plombeer.task3.interfaces;

/**
 * Created by plombeer on 07.07.16.
 */
public interface RoverCommand {
    void execute();
}
