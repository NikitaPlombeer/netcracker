package com.plombeer.task3.interfaces;

import com.plombeer.task3.Direction;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Turnable {
    void turnTo(Direction direction);
}
