package com.plombeer.task3.interfaces;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Moveable {
    void move(int dx, int dy);
}
