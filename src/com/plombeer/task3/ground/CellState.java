package com.plombeer.task3.ground;

/**
 * Created by plombeer on 07.07.16.
 */
public enum CellState {
    FREE, OCCUPIED
}
