package com.plombeer.task3.ground;

/**
 * Created by plombeer on 07.07.16.
 */
public class GroundCell {

    private CellState state;
    private int x;
    private int y;

    public GroundCell(CellState state, int x, int y) {
        this.state = state;
        this.x = x;
        this.y = y;
    }

    public CellState getState() {
        return state;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
