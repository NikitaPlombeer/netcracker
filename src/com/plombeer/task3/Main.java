package com.plombeer.task3;

import com.plombeer.task3.ground.Ground;

public class Main {

    public static void main(String[] args) {
        Rover r = new Rover();
        r.getVisor().setGround(new Ground(10, 10));
        r.executeProgramFile("algo.txt");
    }
}
