package com.plombeer.task3.command;

import com.plombeer.task3.Direction;
import com.plombeer.task3.Rover;
import com.plombeer.task3.interfaces.RoverCommand;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by plombeer on 07.07.16.
 */
public class RoverCommandParser {

    private ArrayList<RoverCommand> commands;
    private int position;

    public RoverCommandParser(String filename, Rover rover) {
        commands = new ArrayList<>();
        position = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));

            String line;
            while((line = in.readLine())!= null){
                String[] s = line.split(" ");
                if(s.length == 3 && s[0].equals("move"))
                    try {
                        commands.add(new MoveCommand(Integer.parseInt(s[1]), Integer.parseInt(s[2]), rover));
                    }catch (NumberFormatException e){
                        System.err.println("Параметром сдвига ровера должен быть Int("+e.getMessage()+") в файле "+filename);
                    }
                else {
                    if(s.length == 2 && s[0].equals("turn")) {
                        try {
                            commands.add(new TurnCommand(Direction.valueOf(s[1].toUpperCase()), rover));
                        }catch (IllegalArgumentException e){
                            System.err.println("Неправильный параментр направления("+e.getMessage()+") в файле "+filename);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл с алгоритмом не найден");
        } catch (IOException e) {
            System.out.println("Ошибка чтения");
        }
    }

    public boolean hasNext(){
        return position < commands.size();
    }

    public RoverCommand readNextCommand(){
        return commands.get(position++);
    }
}
