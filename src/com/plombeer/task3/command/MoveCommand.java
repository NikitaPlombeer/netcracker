package com.plombeer.task3.command;

import com.plombeer.task3.interfaces.Moveable;
import com.plombeer.task3.interfaces.RoverCommand;

/**
 * Created by plombeer on 07.07.16.
 */
public class MoveCommand implements RoverCommand {

    private int x;
    private int y;
    private Moveable moveable;

    public MoveCommand(int x, int y, Moveable moveable) {
        this.x = x;
        this.y = y;
        this.moveable = moveable;
    }

    @Override
    public void execute() {
        moveable.move(x, y);
    }
}
