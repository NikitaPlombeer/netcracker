package com.plombeer.task1.interfaces;

import com.plombeer.task1.Direction;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Turnable {
    void turnTo(Direction direction);
}
