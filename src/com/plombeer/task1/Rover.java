package com.plombeer.task1;

import com.plombeer.task1.interfaces.Moveable;
import com.plombeer.task1.interfaces.Turnable;

/**
 * Created by plombeer on 05.07.16.
 */
public class Rover implements Moveable, Turnable {

    private Direction direction;
    private int x;
    private int y;

    public Rover(int x, int y) {
        this.x = x;
        this.y = y;
        direction = Direction.NORTH;
    }

    @Override
    public void move(int dx, int dy) {
        x += dx;
        y += dy;
    }

    @Override
    public void turnTo(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Rover{" +
                "direction=" + direction +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}