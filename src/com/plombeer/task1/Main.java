package com.plombeer.task1;

public class Main {

    public static void main(String[] args) {
        Rover rover = new Rover(1, 2);
        rover.turnTo(Direction.EAST);
        System.out.println(rover);
    }
}
