package com.plombeer.task4.interfaces;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Moveable {
    void move(int dx, int dy);
}
