package com.plombeer.task4.interfaces;

/**
 * Created by plombeer on 07.07.16.
 */
public interface RoverCommand {
    void execute();
}
