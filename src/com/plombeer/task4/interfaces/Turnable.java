package com.plombeer.task4.interfaces;

import com.plombeer.task4.Direction;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Turnable {
    void turnTo(Direction direction);
}
