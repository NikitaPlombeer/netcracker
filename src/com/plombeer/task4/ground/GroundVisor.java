package com.plombeer.task4.ground;

/**
 * Created by plombeer on 07.07.16.
 */
public class GroundVisor {

    private Ground ground;

    public boolean hasObstacles(int x, int y){
        if(x < 0 || x >= ground.getWidth() || y < 0 || y >= ground.getLength())
            throw new GroundVisorExpetion("Вы вышли за границы ("+x+","+y+")");

        return ground.get(x, y).getState().equals(CellState.OCCUPIED);
    }

    public void setGround(Ground ground) {
        this.ground = ground;
    }

    public Ground getGround() {
        return ground;
    }
}
