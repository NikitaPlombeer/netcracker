package com.plombeer.task4.command;

import com.plombeer.task4.Direction;
import com.plombeer.task4.interfaces.RoverCommand;
import com.plombeer.task4.interfaces.Turnable;

/**
 * Created by plombeer on 07.07.16.
 */
public class TurnCommand implements RoverCommand {

    private Direction direction;
    private Turnable turnable;

    public TurnCommand(Direction direction, Turnable turnable) {
        this.direction = direction;
        this.turnable = turnable;
    }

    @Override
    public void execute() {
        turnable.turnTo(direction);
    }
}
