package com.plombeer.task2;

public class Main {

    public static void main(String[] args) {
        Rover r = new Rover();
        r.getVisor().setGround(new Ground(10, 10));
        r.turnTo(Direction.EAST);
        r.move(9, 9);
    }
}
