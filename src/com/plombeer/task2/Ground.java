package com.plombeer.task2;

/**
 * Created by plombeer on 07.07.16.
 */
public class Ground {

    private GroundCell[][] landscape;
    private int length;
    private int width;

    public Ground(int length, int width) {
        this.length = length;
        this.width = width;
        landscape = new GroundCell[width][length];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < length; j++) {
                landscape[i][j] = new GroundCell(CellState.FREE, i, j);
            }
        }
    }

    public int getLength() {
        return length;
    }

    public GroundCell get(int x, int y){
        return landscape[x][y];
    }

    public int getWidth() {
        return width;
    }
}
