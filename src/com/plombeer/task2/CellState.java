package com.plombeer.task2;

/**
 * Created by plombeer on 07.07.16.
 */
public enum CellState {
    FREE, OCCUPIED
}
