package com.plombeer.task2;

/**
 * Created by plombeer on 05.07.16.
 */
public enum Direction {
    NORTH, EAST, SOUTH, WEST
}
