package com.plombeer.task2.interfaces;

import com.plombeer.task2.Direction;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Turnable {
    void turnTo(Direction direction);
}
