package com.plombeer.task5.interfaces;

import com.plombeer.task5.Direction;

/**
 * Created by plombeer on 05.07.16.
 */
public interface Turnable {
    void turnTo(Direction direction);
}
