package com.plombeer.task5;

import com.plombeer.task5.abs.AbsractParser;
import com.plombeer.task5.abs.ParserFactory;
import com.plombeer.task5.ground.GroundVisor;
import com.plombeer.task5.ground.GroundVisorExpetion;
import com.plombeer.task5.interfaces.Moveable;
import com.plombeer.task5.interfaces.ProgramFileAware;
import com.plombeer.task5.interfaces.Turnable;

/**
 * Created by plombeer on 05.07.16.
 */
public class Rover implements Moveable, Turnable, ProgramFileAware {

    private Direction direction;
    private int x;
    private int y;
    private GroundVisor visor;

    public Rover(int x, int y) {
        this.x = x;
        this.y = y;
        direction = Direction.NORTH;
        visor = new GroundVisor();
    }

    public Rover() {
        this(0, 0);
    }

    @Override
    public void move(int dx, int dy) {
        int newX = x + dx;
        int newY = y + dy;

        if(visor.hasObstacles(newX, newY))
            throw new GroundVisorExpetion("Наткнулись на препятсвие");

        x = newX;
        y = newY;
        System.out.println("Move to("+x+","+y+")");
    }

    public GroundVisor getVisor() {
        return visor;
    }

    @Override
    public void turnTo(Direction direction) {
        this.direction = direction;
        System.out.println("Turn to "+direction.toString());
    }

    @Override
    public void executeProgramFile(String filename){
        ParserFactory factory = ParserFactory.newInstance();
        AbsractParser parser = factory.getParser(filename, this);
        while(parser.hasNext())
            parser.readNextCommand().execute();
    }

    @Override
    public String toString() {
        return "Rover{" +
                "direction=" + direction +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}