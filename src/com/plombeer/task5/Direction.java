package com.plombeer.task5;

/**
 * Created by plombeer on 05.07.16.
 */
public enum Direction {
    NORTH, EAST, SOUTH, WEST
}
