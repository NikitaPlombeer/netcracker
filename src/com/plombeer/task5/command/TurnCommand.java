package com.plombeer.task5.command;

import com.plombeer.task5.Direction;
import com.plombeer.task5.interfaces.RoverCommand;
import com.plombeer.task5.interfaces.Turnable;

/**
 * Created by plombeer on 07.07.16.
 */
public class TurnCommand implements RoverCommand {

    private Direction direction;
    private Turnable turnable;

    public TurnCommand(Direction direction, Turnable rover) {
        this.direction = direction;
        this.turnable = rover;
    }

    @Override
    public void execute() {
        turnable.turnTo(direction);
    }
}
