package com.plombeer.task5.command;

import com.plombeer.task5.interfaces.Moveable;
import com.plombeer.task5.interfaces.RoverCommand;

/**
 * Created by plombeer on 07.07.16.
 */
public class MoveCommand implements RoverCommand {

    private int x;
    private int y;
    private Moveable moveable;

    public MoveCommand(int x, int y, Moveable rover) {
        this.x = x;
        this.y = y;
        this.moveable = rover;
    }

    @Override
    public void execute() {
        moveable.move(x, y);
    }
}
