package com.plombeer.task5.command;

import com.plombeer.task5.Rover;
import com.plombeer.task5.interfaces.RoverCommand;

/**
 * Created by plombeer on 07.07.16.
 */
public class ImportCommand implements RoverCommand {

    private String filename;
    private Rover rover;

    public ImportCommand(String filename, Rover rover) {
        this.filename = filename;
        this.rover = rover;
    }

    @Override
    public void execute() {
        rover.executeProgramFile(filename);
    }
}
