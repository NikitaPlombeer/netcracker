package com.plombeer.task5.command;


import com.plombeer.task5.interfaces.RoverCommand;

/**
 * Created by plombeer on 07.07.16.
 */
public class LoggingCommand implements RoverCommand {

    private RoverCommand command;

    public LoggingCommand(RoverCommand command) {
        this.command = command;
    }

    @Override
    public void execute() {
        System.out.println("Вызываем "+command.getClass().getSimpleName());
        command.execute();
    }
}
