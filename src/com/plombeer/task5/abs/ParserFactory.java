package com.plombeer.task5.abs;

import com.plombeer.task5.Rover;

/**
 * Created by plombeer on 07.07.16.
 */
public class ParserFactory {

    public static ParserFactory newInstance() {
        return new ParserFactory();
    }

    public AbsractParser getParser(String filename, Rover rover){
        if(filename.endsWith(".txt"))
            return new TextRoverCommandParser(filename, rover);
        if(filename.endsWith(".xml"))
            return new XmlRoverCommandParser(filename, rover);
        return null;
    }

}
