package com.plombeer.task5.abs;

import com.plombeer.task5.Rover;
import com.plombeer.task5.interfaces.RoverCommand;

import java.util.ArrayList;

/**
 * Created by plombeer on 07.07.16.
 */
public abstract class AbsractParser {

    protected ArrayList<RoverCommand> commands;
    private int position;
    protected final Rover rover;

    public AbsractParser(String filename, Rover rover) {
        this.rover = rover;
        commands = new ArrayList<>();
        position = 0;
        parse(filename);
    }

    protected abstract void parse(String filename);

    public boolean hasNext(){
        return position < commands.size();
    }

    public RoverCommand readNextCommand(){
        return commands.get(position++);
    }
}
