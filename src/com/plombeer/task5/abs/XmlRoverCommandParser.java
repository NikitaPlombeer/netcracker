package com.plombeer.task5.abs;

import com.plombeer.task5.Direction;
import com.plombeer.task5.Rover;
import com.plombeer.task5.command.ImportCommand;
import com.plombeer.task5.command.LoggingCommand;
import com.plombeer.task5.command.MoveCommand;
import com.plombeer.task5.command.TurnCommand;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by plombeer on 07.07.16.
 */
public class XmlRoverCommandParser extends AbsractParser{

    public XmlRoverCommandParser(String filename, Rover rover) {
        super(filename, rover);
    }

    @Override
    protected void parse(String filename) {
        DocumentBuilderFactory e = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = e.newDocumentBuilder();
            Document document = builder.parse(filename);

            NodeList childNodes =  document.getFirstChild().getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node item = childNodes.item(i);
                if(item.getNodeName().equals("move")){
                    NamedNodeMap attributes = item.getAttributes();
                    try {
                        int x = Integer.parseInt(attributes.getNamedItem("x").getNodeValue());
                        int y = Integer.parseInt(attributes.getNamedItem("y").getNodeValue());
                        commands.add(new LoggingCommand(new MoveCommand(x, y, rover)));
                    }catch (NumberFormatException e1){
                        System.err.println("Параметром сдвига ровера должен быть Int("+e1.getMessage()+") в файле "+filename);
                    }
                } else
                if(item.getNodeName().equals("turn")){
                    NamedNodeMap attributes = item.getAttributes();
                    String direction = attributes.getNamedItem("direction").getNodeValue().toUpperCase();
                    try {
                        commands.add(new LoggingCommand(new TurnCommand(Direction.valueOf(direction), rover)));
                    }catch (IllegalArgumentException e1){
                        System.err.println("Неправильный параментр направления("+e1.getMessage()+") в файле "+filename);
                    }
                } else
                if(item.getNodeName().equals("import")){
                    NamedNodeMap attributes = item.getAttributes();
                    String _filename = attributes.getNamedItem("filename").getNodeValue();
                    commands.add(new LoggingCommand(new ImportCommand(_filename, rover)));
                }
            }
        } catch (ParserConfigurationException e1) {
            System.out.println("ParserConfigurationException: "+e1.getMessage());
        } catch (SAXException | IOException e1) {
            System.out.println("Some troubles: "+e1.getMessage());

        }
    }
}
