package com.plombeer.task5.abs;

import com.plombeer.task5.Direction;
import com.plombeer.task5.Rover;
import com.plombeer.task5.command.ImportCommand;
import com.plombeer.task5.command.LoggingCommand;
import com.plombeer.task5.command.MoveCommand;
import com.plombeer.task5.command.TurnCommand;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by plombeer on 07.07.16.
 */
public class TextRoverCommandParser extends AbsractParser{

    public TextRoverCommandParser(String filename, Rover rover) {
        super(filename, rover);
    }

    @Override
    protected void parse(String filename) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));

            String line;
            while((line = in.readLine())!= null){
                if(line.startsWith("import ")){
                    commands.add(new LoggingCommand(new ImportCommand(line.substring(7), rover)));
                    continue;
                }
                String[] s = line.split(" ");
                if(s.length == 3 && s[0].equals("move"))
                    try {
                        commands.add(new LoggingCommand(new MoveCommand(Integer.parseInt(s[1]), Integer.parseInt(s[2]), rover)));
                    }catch (NumberFormatException e){
                        System.err.println("Параметром сдвига ровера должен быть Int("+e.getMessage()+") в файле "+filename);
                    }
                else {
                    if(s.length == 2 && s[0].equals("turn")) {
                        try {
                            commands.add(new LoggingCommand(new TurnCommand(Direction.valueOf(s[1].toUpperCase()), rover)));
                        }catch (IllegalArgumentException e){
                            System.err.println("Неправильный параментр направления("+e.getMessage()+") в файле "+filename);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Ошибка чтения");
        }
    }
}
