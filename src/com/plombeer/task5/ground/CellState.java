package com.plombeer.task5.ground;

/**
 * Created by plombeer on 07.07.16.
 */
public enum CellState {
    FREE, OCCUPIED
}
