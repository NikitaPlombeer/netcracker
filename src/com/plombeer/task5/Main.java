package com.plombeer.task5;

import com.plombeer.task5.ground.Ground;

public class Main {

    public static void main(String[] args) {
        Rover r = new Rover();
        r.getVisor().setGround(new Ground(20, 20));
        r.executeProgramFile("algo3.xml");
    }
}
